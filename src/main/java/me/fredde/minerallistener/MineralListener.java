package me.fredde.minerallistener;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.StatusChangeEvent;
import net.dv8tion.jda.core.hooks.EventListener;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import javax.security.auth.login.LoginException;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.UUID;

class MineralListener extends ListenerAdapter implements EventListener, Listener
{

    private JDA jda;
    private boolean ready;

    private EmbedBuilder builder = new EmbedBuilder().setColor(Color.CYAN);
    private HashMap<UUID, Integer> users = new HashMap<>();
    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM HH:mm:ss");

    private static MineralListener instance = new MineralListener();

    static MineralListener getInstance()
    {
        return instance;
    }

    private MineralListener()
    {
    }

    void initialize(JavaPlugin plugin, String token, long channelID, int minutes)
    {
        try
        {
            jda = new JDABuilder(token).addEventListener(this).build();
        } catch (LoginException e)
        {
            e.printStackTrace();
        }

        final int interval = 20 * 60 * minutes;

        new BukkitRunnable()
        {

            @Override
            public void run()
            {
                if (ready && users.size() > 0)
                {
                    StringJoiner message = new StringJoiner("\n");

                    for (Map.Entry<UUID, Integer> entry : users.entrySet())
                    {
                        message.add(String.format("%s: %d diamond(s)",
                                Bukkit.getOfflinePlayer(entry.getKey()).getName(),
                                entry.getValue()));
                    }

                    builder.setTitle(String.format("[ML] (%s)", formatter.format(new Date())));
                    builder.setDescription(message.toString());
                    jda.getTextChannelById(channelID).sendMessage(builder.build()).queue();
                }

                users.clear();
            }
        }.runTaskTimer(plugin, interval, interval);
    }

    void shutdown()
    {
        if (jda != null)
        {
            jda.shutdown();
        }
    }

    @Override
    public void onReady(ReadyEvent event)
    {
        super.onReady(event);
        ready = true;
    }

    @Override
    public void onStatusChange(StatusChangeEvent event)
    {
        super.onStatusChange(event);
        ready = event.getNewStatus().equals(JDA.Status.CONNECTED);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event)
    {
        final Block block = event.getBlock();
        final Material material = block.getType();

        if (material.equals(Material.DIAMOND_ORE) && block.getLocation().getBlockY() <= 16)
        {
            final UUID uuid = event.getPlayer().getUniqueId();
            users.put(uuid, users.get(uuid) == null ? 1 : users.get(uuid) + 1);
        }
    }
}
