package me.fredde.minerallistener;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

class YamlManager
{

    private File file;
    private FileConfiguration config;

    YamlManager(String path, String name)
    {
        file = new File(path, name);

        if (!file.exists())
        {
            file.getParentFile().mkdirs();
            try
            {
                file.createNewFile();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        config = new YamlConfiguration();

        try
        {
            config.load(file);
        } catch (IOException | InvalidConfigurationException e)
        {
            e.printStackTrace();
        }
    }

    FileConfiguration getConfig()
    {
        return config;
    }

    void write(String path, Object value)
    {
        config.set(path, value);

        if (file == null)
        {
            return;
        }

        try
        {
            config.save(file);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
