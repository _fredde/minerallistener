package me.fredde.minerallistener;

import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin
{

    @Override
    public void onEnable()
    {
        final Settings settings = getSettings();

        if (settings == null)
        {
            this.getLogger().warning("Settings are invalid, check your 'config.yml'.");
            return;
        }

        this.getServer().getPluginManager().registerEvents(MineralListener.getInstance(), this);
        MineralListener.getInstance().initialize(this, settings.token, settings.channelID, settings.minutes);
    }

    @Override
    public void onDisable()
    {
        MineralListener.getInstance().shutdown();
    }

    private Settings getSettings()
    {
        YamlManager manager = new YamlManager(this.getDataFolder().getPath(), "config.yml");

        final String token = manager.getConfig().getString("token", null);

        if (token == null)
        {
            this.getLogger().warning("Token is not defined in 'config.yml'.");
            manager.write("token", "<TOKEN>");

            return null;
        }

        final long channelID = manager.getConfig().getLong("channelID", 0L);

        if (channelID == 0)
        {
            this.getLogger().warning("channelID is not defined or zero in 'config.yml'.");
            manager.write("channelID", 0L);

            return null;
        }

        final int minutes = manager.getConfig().getInt("minutes", 0);

        if (minutes == 0)
        {
            this.getLogger().warning("minutes is not defined or zero in 'config.yml'.");
            manager.write("minutes", 15);

            return null;
        }

        return new Settings(token, channelID, minutes);
    }

    private class Settings
    {

        String token;
        long channelID;
        int minutes;

        Settings(String token, long channelID, int minutes)
        {
            this.token = token;
            this.channelID = channelID;
            this.minutes = minutes;
        }
    }
}
